/*window.addEventListener("load", function () {
    alert("ya cargo"); escuchando evnetos
});*/
/*
setInterval -> para que se ejecute algo de manera indefinia cierto tiempo
setTimeout -> para que se ejecute una sola vez en un tiempo de terminado

var tiempo = setInterval(function () {
    document.write("texto de ejemplo <br>");
}, 2000);
var tiempo = setTimeout(function () {
    document.write("texto de ejemplo <br>");
}, 2000);
*/
/*
PLAY 
ENDED
SEEKING -> ADELANTAR
var primero = document.querySelector(".uno");

primero.addEventListener("play", function () {
    alert('pusiste play');
});

*/
/*
ALERT
CONFIRM
PROMPT
alert("hola a todos \n como estas");
var a = confirm("necesita ayuda?");

if (a) {
    window.location = "nuevo.html";
} else {
    document.write("dijo que no");
}
var a = prompt("Nombre completo");
if (a) {
    document.write(a);
} else if (a == null) {
    document.write("ud puso cancelar");
}
*/
/*
CASTING
var a = "10.55";
var b = Number(a);
var b = parseInt(a);
var b = parseFloat(a);
Number.istipo_dato -> me dice si el dato si es falso o veradero de ese dato
 document.write(typeof b );
*/
/*
Cantida decimales
var a = "10.5512312311235";
var b = a.toFixed(2);
document.write(b);


*/
/*
var a = 10.542312311235;
var b = a.toFixed(2);
var b = a.toString(2); - > pasar a string

document.write(b);
*/
/*
------------------------
length
search
indexOf
lastIndexOf
match
substr
substring
startWith
endsWith
includes
------------------------
repeat ->repite
replace -> buscar una palabra y que la reemplaze por otra
slice
split
trim
toLowerCase
toUpperCase
concat(,)
${} y con el uso de ´ ´ -> concatenar


OBJETOS(
    propiedades = patas, cabeza ...
    metodos = come, duerme
)


var a = "hola a todos yo soy cristhian fonseca";
document.write(a.indexOf("yo"));-> en donde esta la "yo"
document.write(a.search("yo"));-> en donde esta la "yo"
document.write(a.lastIndexOf("yo")); la ultima vez que nombraron "yo"
var b = a.match(a);
var c = "lunes, martes, miercoles, jueves";
document.write(a.match(/yo/gi));
document.write(a.substr(5, 10)); //cadena apartir de donde le indiaue el numero
document.write(a.substring(5, 10)); //cadena apartir de donde le indiaue el numero contando siempre desde la posicion incial
document.write(a.charAt(5)); //un char en la posiction indicada
document.write(a.startsWith("Hola")); //saber si comienza con ea palabra
document.write(a.endsWith("Hola")); //saber si finaliza con esa palabra
document.write(a.includes("Hola"),20); //que la empieze a buscar desde aqui
document.write(a.repeat(5)); //repita 5 veces
document.write(a.replace("Hola", "quepedo"), 20); //remplaza hola por quepedo
document.write(a.slice(5)); //empieze a impirmir desde 5
document.write(c.split(",")); //transforma a array por caracter que los divida en este caso la ,
*/
/*
ARRAYS 

*/
/*
var semana = new Array("Lunes", "martes");
var semana = ["lunes", "martes", 2131];
var semana = [
    ["10", "27", "30"],
    ["11", "22", "33"],
    ["14", "25", "36"]
]

----------
length 
push
pop ->ultimo valor
join -> array to string
split -> string to array
array.from  (map)
sort
find ->buscar
findIndex -> posicion palabra
filter -> nos trae todas palbras
some ->


----------
var semana = ["lunes", "martes", 2131];

var creacio = semana.from();
var creacio = semana.map(dia => dia.text);
var creacio = semana.map(dia => dia.textContent);
var semana = Array.from(document.querySelector(".dias p"));

var creacio = semana.reverse();

var buscar = uno.filter(dos => dos == "martes");
var buscar = uno.some(dos => dos == "lua");
document.write(buscar);

*/
/*
OBJETOS

*/ /*
function Gato(nombre, edad, peso) {
  this.nombre = nombre;
  this.edad = edad;
  this.peso = peso;
  this.correr = function() {
    document.write("el gato" + nombre + " corre");
  };
}
var gato1 = new Gato("Masias", 12, 4);
var gato2 = new Gato("Melman", 8, 3);
gato1.correr();
*/
/*
class
class Gato {
  constructor(nombre, edad, peso) {
    this.nombre = nombre;
    this.edad = edad;
    this.peso = peso;
  }
  correr(nombre) {
    document.write("El gato corre" + nombre);

  }
}
var gato1 = new Gato("Masias", 12, 4);
var gato2 = new Gato("Melman", 8, 3);

gato1.correr();
*/
/*
HERENCIA

class Animal {
  constructor(tipo, raza) {
    this.cero = tipo;
    this.raza = raza;
  }
}
class Domesticos extends Animal {
  constructor(cero, nombre, edad, peso) {
    super(cero);
    this.uno = nombre;
    this.dos = edad;
    this.tres = peso;
  }
}

var Animal1 = new Domesticos("Hogar", "Boby", 8, 6);
*/

/*
Todo sitio web esta en el BOM y este se divide en :
Frames[]
history
location
navigator
screen
document(DOM) -> aqui esta el contenido
window.name = "ES un nombre de una ventana";

var dato = "El nombre es:" + window.name + "<br>";
var ancho = "el ancho es " + window.outerWidth + "<br>";
var alto = " el alto es " + window.outerHeight + "<br>";
var anchointerno = " el ancho interno es " + window.innerWidth + "<br>";
var altointerno = " el alto interno es " + window.innerHeight + "<br>";
var scrollHorizontal = "El Scroll Horizontal es:" + window.pageXOffset + "<br>";
var distanciaIzquierda =
  "la distacia deesde la izquierda " + window.screenX + "br";
var distanciaSuperior =
  "la distacia deesde la Y " + window.screenY + "br";
document.write(dato, ancho, distanciaIzquierda);

window.alert("hola a todos");
function visitar() {
  ventana = window.open("nuevo.html", "_blank", "width-500, height-300"); //Abre una url
}
function cerrar() {
  ventana.close();
}
function dimension() {
  ventana.resizeBy(10, 10); //configura el tamaño de  10  en 10
}
function dimensionExacta() {
  ventana.resizeTo(500, 500);
}

FRAME
alert(window.length); //Sabemos cuantos frames lo tenemos



console.log(historial);
var historial = window.history;

var localizacion = window.location; //donde esta nuestro archivo
var navegador = window.navigator.vendor; //saber cual es el navegador

Screen
var pantalla = window.screen;


*/
/*
***DOM
traer un elemento del html
var a = document.querySelector(".primero"); //para buscar clases
var a = document.querySelector("#dos"); //buscar id
var a = document.querySelector("h1"); //buscar etiquetas
var a = document.querySelector("a[class='vinculo']"); //buscar en todas las etiquetas la que tenga la class vinculo
var a = document.querySelector("a")[0]; //bsuca todos las etiquetas las pone en un array
var a = document.getElementById("uno"); //busca por id
var a = document.getElementsByClassName("uno")[1]; //busca por class
var a = document.getElementsByTagName("p")[0]; //busca todas las etiquetas
a.innerHTML = "cambio"; //cambia algo por esto
a.innerHTML += "agregad"; //como agregars
a.clasname = a.clasname.replace("primero", "nueva");
*/

var a = document.getElementsByClassName("primero")[0]; //busca todas las etiquetas

a.style.color = "blue";
a.style.border = "1px solid red";
